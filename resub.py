import datetime
backtime = 1000  # millisec

with open("oldsub.srt", "r") as fopen:
    lines = fopen.readlines()
    with open("newsub.srt", "w") as fwrite:
        for line in lines:
            if "-->" not in line:
                fwrite.write(line)
                continue
            timestamp = line.split("-->")
            for i in range(len(timestamp)):
                timestamp[i] = timestamp[i].strip()
                datetimeobj = datetime.datetime.strptime(
                    timestamp[i], '%H:%M:%S,%f')
                #new_dt = datetimeobj.microsecond - 200000
                t1 = datetime.timedelta(hours=datetimeobj.hour,
                                        minutes=datetimeobj.minute,
                                        seconds=datetimeobj.second,
                                        microseconds=datetimeobj.microsecond)
                t2 = datetime.timedelta(
                    hours=0,
                    minutes=0,
                    seconds=2,
                    microseconds=550000,)
                newtime = str(t1-t2)
                timestamp[i] = newtime.replace('.', ',').replace('000', '')
            fwrite.write('{0} --> {1}\n'.format(timestamp[0], timestamp[1]))
